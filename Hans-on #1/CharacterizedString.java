import java.util.Scanner;

public class CharacterizedString{

    Scanner inputUser = new Scanner(System.in);

    public String getKalimat(){
        String kalimat;
        System.out.print("Ketik kata/kalimat, lalu tekan enter : ");
        kalimat = inputUser.nextLine();
        return kalimat;
    }

    public static void main(String[] args){
        CharacterizedString characterizedString = new CharacterizedString();

        String userIn = characterizedString.getKalimat();

        System.out.println();
        System.out.println("#######################################");
        System.out.println("String penuh : " + userIn);
        System.out.println("#######################################");
        System.out.println();
        System.out.println("Proses memecahkan kata/kalimat menjadi karakter . . .");
        System.out.println(". . . . . ");
        char[] karakter = userIn.toCharArray();
        System.out.println("Done.");
        System.out.println("#######################################");
        for (int i = 0; i < karakter.length; i++) {
            System.out.printf("Karakter[%d] : %s ", i, karakter[i]);
            System.out.println();
        }
        System.out.println("#######################################");
    }
}